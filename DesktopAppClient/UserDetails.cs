﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DesktopAppClient.Service;
namespace DesktopAppClient
{
    public partial class UserDetails : Form
    {
        private User user;
        public UserDetails()
        {
            InitializeComponent();
            SaveButton.Visible = true;
            UpdateButton.Visible = false;
        }
        public UserDetails(User user)
        {
            this.user = user;
            InitializeComponent();
            TxtName.Text = user.name;
            TxtSureName.Text = user.lastname;
            TxtEmail.Text = user.email;
            TxtPostCode.Text = user.postCode;
            TxtCity.Text = user.city;
            TxtPhoneNumber.Text = user.phoneNumber;
            TxtBirthDay.Text = user.birthDate.ToString();
            SaveButton.Visible = false;
            UpdateButton.Visible = true;
        }

        private void Save_Click(object sender, EventArgs e)
        {
            this.user = new User();
            this.user.name = TxtName.Text;
            this.user.lastname = TxtSureName.Text;
            this.user.phoneNumber = TxtPhoneNumber.Text;
            this.user.email = TxtEmail.Text;
            this.user.postCode = TxtPostCode.Text;
            this.user.city = TxtCity.Text;
            this.user.birthDate = DateTime.Parse(TxtBirthDay.Text); 

            UserService userService = new UserService();
            if (userService.AddNewUser(this.user))
            {
                this.Close();
            }
            else
            {
                MessageBox.Show("something went wrong");
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            
         
            this.user.name = TxtName.Text;
            this.user.lastname = TxtSureName.Text;
            this.user.phoneNumber = TxtPhoneNumber.Text;
            this.user.email = TxtEmail.Text;
            this.user.postCode = TxtPostCode.Text;
            this.user.city = TxtCity.Text;
            this.user.birthDate = DateTime.Parse(TxtBirthDay.Text);

            UserService userService = new UserService();
            if (userService.UpdateUser(this.user))
            {
                this.Close();
            }
            else
            {
                MessageBox.Show("something went wrong");
            }

        }
    }
}
