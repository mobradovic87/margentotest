﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopAppClient.Service
{
    
    public class UserService
    {
        private string connectionString = Properties.Settings.Default.MargentoConnectionString;

        public UserService()
        {
        }
        public bool AddNewUser(User user)
        {
            int recordsAffected = 0;
          

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = "INSERT INTO [User] (Name,LastName,BirhtDate,PhoneNumber,Email,PostCode,City) VALUES (@Name,@LastName, @BirhtDate,@PhoneNumber," +
                        "@Email,@PostCode,@City)";
                   
                    command.Parameters.AddWithValue("@Name", user.name);
                    command.Parameters.AddWithValue("@LastName", user.lastname);
                    command.Parameters.AddWithValue("@BirhtDate", user.birthDate);
                    command.Parameters.AddWithValue("@PhoneNumber", user.phoneNumber);
                    command.Parameters.AddWithValue("@Email", user.email);
                    command.Parameters.AddWithValue("@PostCode", user.postCode);
                    command.Parameters.AddWithValue("@City", user.city);
                    string tmp = command.CommandText.ToString();
                    foreach (SqlParameter p in command.Parameters)
                    {
                        tmp = tmp.Replace('@' + p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");
                    }


                    Debug.WriteLine(tmp);
                    try
                    {
                        connection.Open();
                        recordsAffected = command.ExecuteNonQuery();

                    }
                    catch (SqlException error)
                    {
                        Debug.WriteLine(error.ToString());
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            if (recordsAffected == 1)
                return true;
            else return false;
        }
        public bool UpdateUser(User user)
        {
            
            int recordsAffected = 0;

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = "UPDATE [User] SET Name = @Name, LastName = @LastName, BirhtDate = @BirhtDate, PhoneNumber = @PhoneNumber," +
                        "Email = @Email, PostCode = @PostCode, City = @City WHERE id = @id";
                    command.Parameters.AddWithValue("@Name", user.name);
                    command.Parameters.AddWithValue("@LastName", user.lastname);
                    command.Parameters.AddWithValue("@BirhtDate", user.birthDate);
                    command.Parameters.AddWithValue("@PhoneNumber", user.phoneNumber);
                    command.Parameters.AddWithValue("@Email", user.email);
                    command.Parameters.AddWithValue("@PostCode", user.postCode);
                    command.Parameters.AddWithValue("@City", user.city);
                    command.Parameters.AddWithValue("@id", Int32.Parse(user.id));
                   
                    try
                    {
                        connection.Open();
                        recordsAffected = command.ExecuteNonQuery();
                       
                    }
                    catch (SqlException error)
                    {
                        Debug.WriteLine(error.ToString());
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            if (recordsAffected == 1)
                return true;
            else return false;
          
        }
        public bool DeleteUser(int id)
        {
            int recordsAffected = 0;
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = "DELETE FROM [User] WHERE id = @id";
                    command.Parameters.AddWithValue("@id", id);

                    try
                    {
                        connection.Open();
                        recordsAffected = command.ExecuteNonQuery();

                    }
                    catch (SqlException error)
                    {
                        Debug.WriteLine(error.ToString());
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            if (recordsAffected == 1)
                return true;
            else return false;
        }
    }
}
