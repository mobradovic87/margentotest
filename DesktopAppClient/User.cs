﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopAppClient
{
    public class User
    {
        public string id;
        public string name;
        public string lastname;
        public string phoneNumber;
        public DateTime birthDate;
        public string email;
        public string postCode;
        public string city;
        public User()
        {

        }
        public User(string id,string name, string lastname, string phoneNumber, string email,  string postCode, string city, DateTime birthDate)
        {
            this.id = id;
            this.name = name;
            this.lastname = lastname;
            this.phoneNumber = phoneNumber;
            this.email = email;
            this.birthDate = birthDate;
            this.postCode = postCode;
            this.city = city;
        }

        
     
    
    }


}
