﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DesktopAppClient.Service;

namespace DesktopAppClient
{
    public partial class Main : Form
    {

        private User selectedUser;
        public Main()
        {
            InitializeComponent();
        }
       
        private void Main_Load_1(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the '_MargentoWebTest_Models_DatabaseContextDataSet.User' table. You can move, or remove it, as needed.
            this.userTableAdapter.Fill(this._MargentoWebTest_Models_DatabaseContextDataSet.User);

            //this.usersTableAdapter1.Fill(margentoDataSet.users);
        }
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserDetails newUser = new UserDetails();
            newUser.FormClosed += new FormClosedEventHandler(Form_Closed);
            newUser.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void OpenUserDetails(object sender, DataGridViewCellEventArgs e)
        {
            UserDetails newUser = new UserDetails(this.selectedUser);
            newUser.FormClosed += new FormClosedEventHandler(Form_Closed);
            newUser.Show();

        }
        //event handler when form is closed
        private void Form_Closed(object sender, FormClosedEventArgs e)
        {
            UserDetails frm = (UserDetails)sender;
            this.userTableAdapter.Fill(this._MargentoWebTest_Models_DatabaseContextDataSet.User);

        }

        private void SelectUser(object sender, DataGridViewCellEventArgs e)
        {
           

            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView2.Rows[e.RowIndex];
                this.selectedUser = new User(row.Cells[0].Value.ToString(), row.Cells[1].Value.ToString(), row.Cells[2].Value.ToString(),
                    row.Cells[3].Value.ToString(), row.Cells[4].Value.ToString(), row.Cells[5].Value.ToString(), row.Cells[6].Value.ToString(), DateTime.Parse(row.Cells[7].Value.ToString()));
            }

        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var confirmResult = MessageBox.Show("Are you sure to delete this item ??",
                                     "Confirm Delete!!",
                                     MessageBoxButtons.YesNo);

            if (confirmResult == DialogResult.No)
            {
                return;
            }
            
            if (this.selectedUser == null)
            {

                MessageBox.Show("Select User");
                return;
            }

            UserService userService = new UserService();
            if (userService.DeleteUser(Int32.Parse(this.selectedUser.id)))
            {
                this.userTableAdapter.Fill(this._MargentoWebTest_Models_DatabaseContextDataSet.User);
            }
            else
            {
                MessageBox.Show("Something vent wrong");
            }
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.selectedUser == null)
            {
                MessageBox.Show("Select User");
                return;
            }

            UserDetails newUser = new UserDetails(this.selectedUser);
            newUser.FormClosed += new FormClosedEventHandler(Form_Closed);
            newUser.Show();
        }
    }
}
